import kotlinx.serialization.Serializable

val entryStorage = mutableListOf<Entry>()

@Serializable
data class Entry(val id: String, var title: String, var text: String)
