package com.jetbrains.handson.routes

import Entry
import entryStorage
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route
import io.ktor.routing.routing

fun Application.registerEntryRoutes() {
    routing {
        entryRouting()
    }
}

fun Route.entryRouting() {
    route("/entry") {
        get {
            if (entryStorage.isNotEmpty()) {
                call.respond(entryStorage)
            } else {
                call.respondText("No entries found", status = HttpStatusCode.NotFound)
            }
        }
        get("{id}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing or malformed id",
                status = HttpStatusCode.BadRequest
            )
            val customer =
                entryStorage.find { it.id == id } ?: return@get call.respondText(
                    "No entry with id $id",
                    status = HttpStatusCode.NotFound
                )
            call.respond(customer)
        }
        post {
            val entry = call.receive<Entry>()
            if (entryStorage.find { it.id == entry.id } != null) {
                return@post call.respondText(
                    "Entry with id ${entry.id} already exists",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                entryStorage.add(entry)
                call.respondText("Entry stored correctly", status = HttpStatusCode.Created)
            }
        }
        put("{id}") {
            val updatedEntry = call.receive<Entry>()
            if (entryStorage.find { it.id == updatedEntry.id } == null) {
                return@put call.respondText(
                    "Entry with id ${updatedEntry.id} does not exists",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                entryStorage.find { it.id == updatedEntry.id }?.let {
                    it.title = updatedEntry.title
                    it.text = updatedEntry.text
                }
                call.respondText("Entry updated correctly", status = HttpStatusCode.Created)
            }
        }
        delete("{id}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if (entryStorage.removeIf { it.id == id }) {
                call.respondText("Entry removed correctly", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("Not Found", status = HttpStatusCode.NotFound)
            }
        }
    }
}
